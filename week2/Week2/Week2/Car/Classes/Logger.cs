﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Week2.Car.Classes
{
    public class Logger
    {
        private const string FILE = "output.log";

        /* 
         * 1 for Console log 
         * 2 for File log
         */
        private int LoggingType { get; set; }

        public Logger()
        {
            LoggingType = 1;
        }

        public Logger(int logType)
        {
            LoggingType = logType;
        }
        public void Log(string message)
        {

            if (LoggingType == 1)
            {
                Console.WriteLine(message);
            }

            if (LoggingType == 2)
            {
                StreamWriter stream = new System.IO.StreamWriter(FILE, true);
                stream.WriteLine(message);
                stream.Close();
            }
        }
    }
}
