﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week2.Car.Classes
{
    public class Producer
    {
        private string Name { get; set; }

        public Producer(string name)
        {
            Name = name;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
