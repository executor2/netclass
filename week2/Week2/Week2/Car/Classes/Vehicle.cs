﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week2.Car.Classes
{
    public class Vehicle
    {
        public Vehicle()
        {
            Name = new Producer("");
            Model = "";
            Year = 0;
        }
        public Vehicle(string name, string model, int year)
        {
            Name = new Producer(name);
            Model = model;
            Year = year;
        }

        public Producer Name { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }

        override public string ToString()
        {
            return Name + " " + Model + " " + Year;
        }
    }
}
