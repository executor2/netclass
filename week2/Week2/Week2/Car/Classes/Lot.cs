﻿using System;
using System.Collections.Generic;

namespace Week2.Car.Classes
{
    public class Lot
    {
        private List<Vehicle> vehicles = new List<Vehicle>();
        public string Name { get; set; }

        public void AddVehicle(Vehicle vehicle)
        {
            vehicles.Add(vehicle);
        }

        public void RemoveVehicle(Vehicle vehicle)
        {
            vehicles.Remove(vehicle);
        }
    }
}