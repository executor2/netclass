﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week2.Car.Classes
{
    public class ParkingLot
    {
        private List<Lot> lots = new List<Lot>();

        public string Name { get; set; }

        public ParkingLot()
        {
            Lot lot = new Lot();

            Vehicle v1 = new Vehicle("Ford", "Fiesta", 2020);
            Vehicle v2 = new Vehicle("Ford", "TOURNEO", 2020);
            Vehicle v3 = new Vehicle("Ford", "ECOSPORT", 2020);
            Vehicle v4 = new Vehicle("Ford", "PUMA", 2020);

            lot.AddVehicle(v1); lot.AddVehicle(v2); lot.AddVehicle(v3); lot.AddVehicle(v4);
        }
    }
}
