﻿using System;
using System.Collections.Generic;
using System.Text;
using Week2.Car.Interfaces;

namespace Week2.Car.Classes
{
    public class Store: IStore
    {
        private List<Person> persons = new List<Person>();
        private List<Vehicle> inventory = new List<Vehicle>();
        private Dictionary<Person, Vehicle> orders = new Dictionary<Person, Vehicle>();
        private Logger log = new Logger(1);

        public string Name { get; set; }
        public string City { get; set; }
        public int Delivery { get; set; }
        
        public Store()
        {
            Vehicle fordFocus = new Vehicle("Ford", "Focus", 2015);

            inventory.Add(fordFocus);
        }
        public void AddPerson(Person person)
        {
            this.persons.Add(person);
        }

        public void MakeOrder(Person alex)
        {
            log.Log("Our inventary is :");

            foreach (Vehicle vehicle in inventory)
            {
                log.Log(vehicle.ToString());
                // We would ask the customer the index of the vehicle he wants , but we use only 1 car
            }

            orders.Add(alex, inventory[0]);
            log.Log($"Your delivery will be in {Delivery} weeks");
        }

        public void CancelOrder(Person person)
        {
            orders.Remove(person);
            log.Log($"Your order for {inventory[0]} has been cancelled");
        }

        public void RemovePerson(Person alex)
        {
            persons.Remove(alex);
        }
    }
}
