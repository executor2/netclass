﻿using System;
using System.Collections.Generic;
using System.Text;
using Week2.Car.Classes;

namespace Week2.Car.Interfaces
{
    public class Order
    {
        public string Name { get; set; }

        public Order()
        {
            Name = "";
        }

        public Order(string name)
        {
            Name = name;
        }
    }
}
