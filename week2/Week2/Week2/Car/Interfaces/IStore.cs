﻿using Week2.Car.Classes;

namespace Week2.Car.Interfaces
{
    interface IStore
    {
        public void AddPerson(Person person);
        public void MakeOrder(Person person);
        public void CancelOrder(Person person);
        public void RemovePerson(Person person);
    }
}
