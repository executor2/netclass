﻿using System;
using Week2.Car.Classes;

namespace Week2.Car
{
    class MainCarShop
    {
        static Logger log = new Logger(1);

         static void Main(string[] args)
        {
            Person alex = new Person()
            {
                Nume = "Alex",
                Prenume = "Stratan"
            };

            Store fordStore = new Store()
            {
                Name = "FordStore",
                City = "Bucuresti",
                Delivery = 4
            };

            Store skodaStore = new Store()
            {
                Name = "SkodaStore ",
                City = "Bucuresti",
                Delivery = 3
            };

            fordStore.AddPerson(alex);
            fordStore.MakeOrder(alex);
            fordStore.RemovePerson(alex);

            skodaStore.AddPerson(alex);
            skodaStore.MakeOrder(alex);
            skodaStore.RemovePerson(alex);

            fordStore.CancelOrder(alex);
            log.Log("Your delivery has arrived!");
        }
        


    }
}
