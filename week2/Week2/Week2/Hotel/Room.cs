﻿using System;
using System.Collections.Generic;

namespace Week2.Hotel
{
    public class Room
    {
        private string name;
        private List<Rate> rates = new List<Rate>();
        private int adults;
        private int children;

        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        public void AddRate(Rate rate)
        {
            this.rates.Add(rate);
        }

        public int Adults
        {
            get { return this.adults; }
            set { this.adults = value; }
        }

        public int Children
        {
            get { return this.children; }
            set { this.children = value; }
        }

        public double GetPriceforDays(int numberOfDays)
        {
            return this.GetBestPrice() * numberOfDays;

        }

        private double GetBestPrice()
        {
            double bestPrice = double.MaxValue;

            foreach(Rate rate in rates)
            {
                if(rate.Amount <= bestPrice)
                {
                    bestPrice = rate.Amount;
                }
            }

            return bestPrice;
        }

        public string Print()
        {
            string result = $"Name: {name} Adults: {adults} Children: {children}";

            foreach (Rate rate in this.rates)
            {
                result += $"{Environment.NewLine}Rate: { rate.Print()}";
            }
            return result;
        }
    }
}
