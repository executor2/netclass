﻿using System;

namespace Week2.Hotel
{
    public class Rate
    {
        private double amount;
        private string currency;

        public double Amount
        {
            get { return this.amount; }
            set { this.amount = value; }
        }

        public string Currency {
            get { return this.currency; }
            set { this.currency = value; } 
        }

        public string Print()
        {
            return $"Amount: {this.amount} Currency: {this.currency}";
        }
    }
}
