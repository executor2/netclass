﻿using System;
using System.Collections.Generic;

namespace Week2.Hotel
{
    public class HotelClass
    {
        private string name;
        private string city;
        private List<Room> rooms = new List<Room>();

        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        public string City
        {
            get { return this.city; }
            set { this.city = value; }
        }

        public void AddRoom(Room room)
        {
            this.rooms.Add(room);            
        }

        public double[] GetPriceForNumberOfRooms(int numberOfRooms)
        {
            
            if (this.rooms.Count <= numberOfRooms)
            {
                double[] prices = new double[numberOfRooms];

                for (int i = 0; i < numberOfRooms; i++)
                {
                    Room current = this.rooms[i];
                    prices[i] = current.GetPriceforDays(1);
                }

                return prices;
            }
                        
            return new double[0];
        }

        public string Print()
        {
            string result = $"Name: {name} City: {city}";

            foreach(Room room in this.rooms)
            {
               result+= Environment.NewLine +  room.Print();
            }

            return result;
        }

    }
}