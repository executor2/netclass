﻿using System;
using Week4.Interfaces;

namespace Week4.Classes
{
    public class SystemClock : IClock
    {
        public DateTime Now { get; set; }
        public DateTime UtcNow { get; set; }
        public BusinessDate Today { get; set; }

        public SystemClock()
        {

        }

        public SystemClock(DateTime date)
        {
            Now = date.Date;
            UtcNow = date.Date;
            
            BusinessDate today = new BusinessDate();
            today.Date = date;
            Today = today;
        }

        public SystemClock(int year, int month, int day, int hour, int minute, int second)
        {
            DateTime date = new DateTime(year, month, day, hour, minute, second);

            BusinessDate businessDate = new BusinessDate();
            businessDate.Date = date;

            Now = date;
            UtcNow = date;
            Today = businessDate;
        }

    }
}
