﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Week4.Interfaces
{
    public interface IClock
    {
        DateTime Now { get; }

        DateTime UtcNow { get; }

        BusinessDate Today { get; }
    }

    public struct BusinessDate : IFormattable, IEquatable<BusinessDate>, IComparable<BusinessDate>, IXmlSerializable
    {
        public DateTime Date { get; set; }
        public int Year { get { return Date.Year; } }
        public int Month { get { return Date.Month; } }
        public int Day { get { return Date.Day; } }

        public BusinessDate(int year, int month, int day)
        {
            Date = new DateTime(year, month, day);
        }


        public int CompareTo([AllowNull] BusinessDate other)
        {
            return this.Equals(other) ? 1 : 0;
        }

        public bool Equals([AllowNull] BusinessDate other)
        {
            return Date.ToString() == other.ToString();
        }

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            Date = reader.ReadElementContentAsDateTime();
        }

        public string ToString(string format, IFormatProvider formatProvider)
        {
            return Date.ToString();
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteString(Date.ToString());
        }

        public static BusinessDate ParseFromIso8601String(string date)
        {
            string[] dates = date.Split("-");
            return new BusinessDate(int.Parse(dates[0]), int.Parse(dates[1]), int.Parse(dates[2]));
        }

        public static bool operator ==(BusinessDate first, BusinessDate second)
        {
            return first.Equals(second);
        }

        public static bool operator !=(BusinessDate first, BusinessDate second)
        {
            return !first.Equals(second);
        }
    }


}
