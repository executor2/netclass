﻿using System;
using System.Text.RegularExpressions;
using Week4.Classes;
using Week4.Interfaces;

namespace Week4
{
    class Program
    {
        static void Main(string[] args)
        {
            String text = "karunya123.edu  , www.karunya.edu, www.karunya.edu,  http://karunya.edu, https://karunya.edu, www.karunyauniversity.in  ,  https://mykarunya.edu, https://www.karunya.edu  ,  google.com,  google.co.in, www.google.com,  https://www.gmail.com, gmail.com";

            AllUrls(text);
            AllUrlsWithHttps(text);
            AllURrlEndingWithEdu(text);
            ReplaceVowels(text,"t");
            ReplaceNumbers(text);
            DisplayDuplicates(text);
            
            Console.WriteLine("7.Concatenate any two URLs");
            Console.WriteLine("8.Given any URL, display last occurence of any repeating character");
            Console.WriteLine("9.Insert [URL] at the beginning of URLs");
            Console.WriteLine("10.Find out first occurence of character in given url");
            Console.WriteLine("11.List out all the URLs with substring 'oo' in it.");
        }

        private static void DisplayDuplicates(string text)
        {
            Console.WriteLine("6.Display all duplicate URLs");
            AllUrls(text,"(http(s)*://(www)*.\\w+.\\w+)(\\1)*");
        }

        private static void ReplaceNumbers(string text)
        {
            Console.WriteLine("5.Replace all the numbers in the URL with 1 and display");
            Regex regex = new Regex("[0-9]+");
            string match = regex.Replace(text, "1");
            
            Console.WriteLine(match);
        }

        private static void AllUrls(string text, string regexString = "http(s)*://(www)*.\\w+.\\w+")
        {
            Regex regex = new Regex(regexString);
            MatchCollection matches = regex.Matches(text);

            foreach (var match in matches)
            {
                Console.WriteLine(match);
            }

            Console.WriteLine();
        }

        private static void AllUrlsWithHttps(string text)
        {
            Console.WriteLine("2.Display all the URLs which start with https://");
            AllUrls(text, "https://(www)*.\\w+.\\w+");
        }

        private static void AllURrlEndingWithEdu(string text)
        {
            Console.WriteLine("3.URLs ending with .edu");
            AllUrls(text, "http(s)*://(www)*.\\w+.edu");
        }

        private static void ReplaceVowels(string text, string v)
        {
            Console.WriteLine("4.Replace all the vowels in url with given character");

            Regex regex = new Regex("[aeiouAEIOU]+");
            string matches = regex.Replace(text,v);

            Console.WriteLine(matches);
        }
    }
}
