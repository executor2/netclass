﻿namespace LinqAndLamdaExpressions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Models;

    internal class Program
    {
        private static void Main(string[] args)
        {
            List<User> allUsers = ReadUsers("users.json");
            List<Post> allPosts = ReadPosts("posts.json");

            #region Demo

            // 1 - find all users having email ending with ".net".
            var users1 = from user in allUsers
                         where user.Email.EndsWith(".net")
                         select user;

            var users11 = allUsers.Where(user => user.Email.EndsWith(".net"));

            IEnumerable<string> userNames = from user in allUsers
                                            select user.Name;

            var userNames2 = allUsers.Select(user => user.Name);

            foreach (var value in userNames2)
            {
                // Console.WriteLine(value);
            }

            IEnumerable<Company> allCompanies = from user in allUsers
                                                select user.Company;

            var users2 = from user in allUsers
                         orderby user.Email
                         select user;

            var netUser = allUsers.First(user => user.Email.Contains(".net"));
            // Console.WriteLine(netUser.Username);



            // 2 - find all posts for users having email ending with ".net".
            IEnumerable<int> usersIdsWithDotNetMails = from user in allUsers
                                                       where user.Email.EndsWith(".net")
                                                       select user.Id;

            IEnumerable<Post> posts = from post in allPosts
                                      where usersIdsWithDotNetMails.Contains(post.UserId)
                                      select post;

            foreach (var post in posts)
            {
                // Console.WriteLine(post.Id + " " + "user: " + post.UserId);
            }

            IEnumerable<User> allUsersLINQ = from user in allUsers
                                             select user;
            #endregion

            // 3 - print number of posts for each user.
            var numberOfPosts = from user in allUsers
                                from post in allPosts
                                where post.UserId == user.Id
                                group post by post.UserId into postGroup
                                select new
                                {
                                    userID = postGroup.Key,
                                    count = postGroup.Count()
                                };

            foreach (var numPosts in numberOfPosts)
            {
                Console.WriteLine($"UserId: {numPosts.userID} count {numPosts.count}");
            }

            // 4 - find all users that have lat and long negative.

            var usersLatLong = from user in allUsers
                               where (user.Address.Geo.Lat < 0 && user.Address.Geo.Lng < 0)
                               select user;

            Console.WriteLine();

            foreach (var user in usersLatLong)
            {
                Console.WriteLine($"Id: {user.Id} Name: {user.Name}");
            }


            // 5 - find the post with longest body.

            var bigBodyPosts = from selectedPost in allPosts
                               where selectedPost.Body == (from postBody in allPosts select postBody.Body).Max()
                               select selectedPost;

            Console.WriteLine();

            foreach (var post in bigBodyPosts)
            {
                Console.WriteLine(post.Title);
            }


            // 6 - print the name of the employee that have post with longest body.
            var employee = from user in allUsers
                           where user.Id == bigBodyPosts.First().UserId
                           select user;
            Console.WriteLine(employee.First().Name);


            // 7 - select all addresses in a new List<Address>. print the list.
            List<Address> allAddresses = (from address in allUsers
                                          select address.Address).ToList();

            Console.WriteLine();

            // 8 - print the user with min lat

            var minLat = from user in allUsers
                         where user.Address.Geo.Lat == (from latMin in allUsers select latMin.Address.Geo.Lat).Min()
                         select user;
            Console.WriteLine(minLat.First().Name);

            // 9 - print the user with max long
            var maxLong = from user in allUsers
                          where user.Address.Geo.Lng == (from latMin in allUsers select latMin.Address.Geo.Lng).Max()
                          select user;
            Console.WriteLine(maxLong.First().Name);

            // 10 - create a new class: public class UserPosts { public User User {get; set}; public List<Post> Posts {get; set} }
            //    - create a new list: List<UserPosts>
            //    - insert in this list each user with his posts only
            
            var listUserPosts = from groupedPosts in allPosts
                                group groupedPosts by groupedPosts.UserId into g
                                select new
                                {
                                    user = from user in allUsers where user.Id == g.Key select user,
                                    posts = g
                                };

            List<UserPosts> lUserPosts = new List<UserPosts>();

            foreach(var userPosts in listUserPosts)
            {

                UserPosts obj = new UserPosts()
                {
                    User = (User)userPosts.user.First(),
                    Posts = (List<Post>) userPosts.posts.ToList()
                };
               
 //                obj.User(userPosts.user);
                // obj.Posts(userPosts.posts);
                
                lUserPosts.Add(obj);
            }

            Console.WriteLine();

            // 11 - order users by zip code

            var orderUserByZipCode = from user in allUsers
                                     orderby user.Address.Zipcode
                                     select user;

            // 12 - order users by number of posts

            var orderUsersByNumberOfPosts = (from user in allUsers
                                            from post in allPosts
                                            where post.UserId == user.Id                                            
                                            group post by post.UserId into groupPosts
                                            select new
                                            {
                                                userId = groupPosts.Key,
                                                postCount = groupPosts.Count()
                                            }).OrderBy(c => c.postCount);

            Console.ReadLine();
        }

        private static List<Post> ReadPosts(string file)
        {
            return ReadData.ReadFrom<Post>(file);
        }

        private static List<User> ReadUsers(string file)
        {
            return ReadData.ReadFrom<User>(file);
        }
    }

    internal class UserPosts
    {
        public User User { get; set; }
        public List<Post> Posts { get; set; }


    }
}
