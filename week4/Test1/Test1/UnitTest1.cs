using Microsoft.VisualStudio.TestTools.UnitTesting;
using Week4.Interfaces;

namespace Test1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void ConstructedAsSpecified()
        {
            var date = new BusinessDate(2014, 3, 28);
            Assert.AreEqual(date.Day, 28);
            Assert.AreEqual(date.Month, 3);
            Assert.AreEqual(date.Year, 2014);
        }

        [TestMethod]
        public void BusinessDateIsEqualToItself()
        {
            var date = new BusinessDate(2015, 10, 06);
            Assert.AreEqual(date, date);
        }

        [TestMethod]
        public void CanBeParsedFromIso8601DateString()
        {
            Assert.AreEqual(BusinessDate.ParseFromIso8601String("2015-06-10"), new BusinessDate(2015, 6, 10));
        }

        public void Equality(BusinessDate left, BusinessDate right, bool areEqual)
        {
            Assert.Equals(left.Equals(right), areEqual);
            Assert.Equals(left == right, areEqual);
            Assert.Equals(left != right, !areEqual);
            Assert.Equals(left.GetHashCode() == right.GetHashCode(), areEqual);
        }
    }
}
