﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Week1
{
    class Homework
    {

        /** HashSet have unique elements */
        public static void Exercise1()
        {
            Console.WriteLine("Enter a string of characters");
            string input = Console.ReadLine();

            HashSet<string> set = new HashSet<string>();
            foreach (char c in input)
            {
                set.Add(c.ToString());
            }
            Console.WriteLine(string.Join("", set));
        }



        public static void Exercise2()
        {
            int[] numbers = { 1, 2, 3, 4, 9, 5, 9, 7, 8, 9, 9, 8, 8, 8, 9, 10, 11, 12, 8, 13, 9, 14, 9 };
            int foundNumber = 0;
            int occurrence = 0;
            int prevOccurrence = 0;

            Array.Sort(numbers);

            for (int i = 0; i < numbers.Length - 1; i++)
            {
                if (numbers[i] == numbers[i + 1])
                {
                    occurrence++;
                }

                if (numbers[i] != numbers[i + 1])
                {
                    if (occurrence > prevOccurrence)
                    {
                        foundNumber = numbers[i];
                    }
                    prevOccurrence = occurrence;
                    occurrence = 0;
                }
            }

            Console.WriteLine(foundNumber);
        }

        public static void Exercise3()
        {
            string input = Console.ReadLine();
            char[] arrayInput = input.ToCharArray();
            int occurence = 0;
            Dictionary<char, int> chars = new Dictionary<char, int>();
            Array.Sort(arrayInput);

            for (int i = 0; i < arrayInput.Length - 1; i++)
            {
                occurence++;
                if (arrayInput[i] != arrayInput[i + 1] || i == arrayInput.Length - 2)
                {
                    if (i == arrayInput.Length - 2)
                    {
                        occurence++;
                    }

                    chars.Add(arrayInput[i], occurence);
                    occurence = 0;
                }
            }

            foreach (char Key in chars.Keys)
            {
                Console.WriteLine("{0} frequence of {1}", Key, chars[Key]);
            }
        }

        internal static void Exercise4()
        {
            LinkedList<string> list = new LinkedList<string>();
            LinkedList<string> second = new LinkedList<string>();

            list.AddLast("a");
            list.AddLast("b");
            list.AddLast("c");

            LinkedListNode<string> current = list.First;


            while (current != null)
            {
                LinkedListNode<string> node = new LinkedListNode<string>(current.Value);
                second.AddFirst(node);

                current = current.Next;
            }

            foreach (string str in second)
            {
                Console.WriteLine(str);
            }

        }


        internal static void Exercise5()
        {
            LinkedList<string> list = new LinkedList<string>();
            LinkedList<string> uniqueList = new LinkedList<string>();

            list.AddLast("a");
            list.AddLast("b");
            list.AddLast("b");
            list.AddLast("c");
            list.AddLast("d");

            LinkedListNode<string> current = list.First;


            while (current != null)
            {
                Console.WriteLine(current.Value);

                uniqueList.AddLast(current.Value);

                LinkedListNode<string> uniqueCurrent = uniqueList.First;
                while (uniqueCurrent.Next != null)
                {
                    if (current.Value == uniqueCurrent.Next.Value)
                    {
                        uniqueCurrent = uniqueCurrent.Next;
                        uniqueList.Remove(uniqueCurrent);
                    }
                }

                current = current.Next;
            }

            foreach (string str in uniqueList)
            {
                Console.WriteLine(str);
            }

        }

        internal static void Exercise6()
        {

        }

        internal static void Exercise7()
        {
            // string[] names = new string[0];

            //string[] names = new string[1];
            //names[0] = "Han Solo";

            //Console.WriteLine(names[0]);

            //int[] numbers = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            //for (int i = 0; i < numbers.Length - 1; i++)
            //{
            //    Console.WriteLine(numbers[i]);
            //}

            //List<string> list = new List<string>();
            //list.Add("Chewbacca");
            //Console.WriteLine(list.First());

            //List<string> characters = new List<string>()
            //{
            //    "Luke Skywalker",
            //    "Han Solo",
            //    "Chewbacca"
            //};
            //characters.Remove("Luke Skywalker");

            //foreach(string str in characters)
            //{
            //    Console.WriteLine(str);
            //}

            //List<string> characters = new List<string>()
            //{
            //    "Luke Skywalker",
            //    "Han Solo",
            //    "Chewbacca"
            //};

            //characters.RemoveAt(2);
            //foreach(string str in characters)
            //{
            //    Console.WriteLine(str);
            //}

            Dictionary<string, int> people = new Dictionary<string, int>();
            people.Add("Catalin", 34);

            Dictionary<string, bool> characters = new Dictionary<string, bool>()
            {
                { "Luke", true },
                { "Han", false },
                { "Chewbacca", false }
            };

            characters.Remove("Han");

            foreach(var str in characters)
            {
                Console.WriteLine(str);
            }

            Queue<int> primes = new Queue<int>();
            primes.Enqueue(2);
            primes.Enqueue(3);
            primes.Enqueue(5);
            primes.Enqueue(7);
            primes.Enqueue(11);

            int total = 0;

            while(primes.Count >0)
            {
                int prime = primes.Dequeue();
                total += prime;                
            }

            Console.WriteLine(total);

            // Ex 6

            Stack<string> films = new Stack<string>();
            films.Push("A New Hope");
            films.Push("The Empire Strikes Back");
            films.Push("Return of the Jedi");
            

            while (films.Count > 0)
            {
                string film = films.Pop();
                Console.WriteLine(film);
            }


            // Ex 7

            LinkedList<string> movies = new LinkedList<string>();
            movies.AddFirst("Avatar");
            LinkedListNode<string> titanic = new LinkedListNode<string>("Titanic");

            movies.AddLast(titanic);
            movies.AddAfter(titanic, "Star Wars: The Force Awakens after 'Titanic'");

            LinkedList<string> droids = new LinkedList<string>();

            droids.AddLast("C-3PO");
            droids.AddLast("AZI-3");
            droids.AddLast("R2-D2");
            droids.AddLast("IG-88");
            droids.AddLast("2-1B");

            droids.Remove("C-3PO");
            
            LinkedListNode<string> r2 = droids.Find("R2-D2");
            droids.Remove(r2);

            droids.RemoveLast();

            // Ex 8



        }
    }
}
