﻿using System;

namespace Week1
{
    class Week1

    {
        static void Main(string[] args)
        {
            //Week1.doExercise1();
            //Week1.doExercise2();
            //Week1.doExercise3();
            //Week1.doExercise4();
            //Week1.doExercise5();
            //Week1.doExercise6();
            //Week1.doExercise7();            
            
            // Homework.Exercise1();
            // Homework.Exercise2();
            // Homework.Exercise3();
            // Homework.Exercise4();
            //Homework.Exercise5();
            //Homework.Exercise6();
            Homework.Exercise7();
        }

        private static void doExercise7()
        {
            int temperature = int.Parse(Console.ReadLine());

            switch(temperature/10)
            {
                case 0:
                    Console.WriteLine("Temp 0-10 then Very Cold weather");
                    break;
                case 1:
                    Console.WriteLine("Temp 10-20 then Cold weather");
                    break;
                case 2:
                    Console.WriteLine("Temp 20-30 then Normal in Temp");
                    break;
                case 3: 
                    Console.WriteLine("Temp 30-40 then Its Hot");
                    break;
                case 4:
                    Console.WriteLine("Temp >=40 then Its Very Hot");
                    break;      
                default:
                    Console.WriteLine("Very hot!");
                    break;
            }
        }

        private static void doExercise1()
        {
            Console.WriteLine("Enter three letters: ");
            char firstChar = Console.ReadKey().KeyChar;
            char secondChar = Console.ReadKey().KeyChar;
            char thirdChar = Console.ReadKey().KeyChar;


            Console.WriteLine("  {2} {1} {0}", firstChar, secondChar, thirdChar);
        }

        private static void doExercise2()
        {
            string firstNumber = Console.ReadLine();
            string secondNumber = Console.ReadLine();

            Console.WriteLine(Week1.Add(int.Parse(firstNumber), int.Parse(secondNumber)));

        }

        private static int Add(int firstNumber, int secondNumber)
        {
            return firstNumber - secondNumber;
        }

        private static void doExercise3()
        {
            string firstCharacter = Console.ReadKey().KeyChar.ToString();
            int number;
            bool isInt = int.TryParse(firstCharacter, out number);

            if (isInt)
            {
                Console.WriteLine(" Character is a int:", firstCharacter);
            }
            else
            {
                string vowels = "aeiouAEIOU";
                int isVowel = vowels.IndexOf(firstCharacter);

                if (isVowel >= 0)
                {
                    Console.WriteLine("Character is a vowel:", firstCharacter);
                }
                else
                {
                    Console.WriteLine("Characters is a string:", firstCharacter);
                }


            }


            Console.WriteLine(firstCharacter);
        }
        private static void doExercise4()
        {
            Console.WriteLine("Input your height in cm");
            string height = Console.ReadLine();
            int intHeight = int.Parse(height);

            if(intHeight <= 150)
            {
                Console.WriteLine("You are a Dwarf");
            }

            if(intHeight > 150 && intHeight <= 180)
            {
                Console.WriteLine("You are a Elf");
            }

            if(intHeight> 180)
            {
                Console.WriteLine("You are a Orc");
            }
        }

        private static void doExercise5()
        {
            Console.WriteLine("Angle of AB");
            string AB = Console.ReadLine();
            Console.WriteLine("Angle of BC");
            string BC = Console.ReadLine();
            Console.WriteLine("Angle of CA");
            string CA = Console.ReadLine();

            int howManyAnglesAreEqual = 0;

            if (AB == BC) howManyAnglesAreEqual++;
            if (BC == CA) howManyAnglesAreEqual++;
            if (CA == AB) howManyAnglesAreEqual++;

            switch(howManyAnglesAreEqual)
            {
                case 0:
                    Console.WriteLine("Scalene triangle");
                    break;
                case 1:
                    Console.WriteLine("Isosceles triangle");
                    break;
                case 3:
                    Console.WriteLine("Equilateral triangle");
                    break;
            }
        }

        private static void doExercise6()
        {
            int number = int.Parse(Console.ReadLine());
            int sum = 0;

            for(int i = 1;  i<=number; i++)
            {
                Console.WriteLine(2*i);
                sum += 2 * i;
            }

            Console.WriteLine(sum);
        }

    }
}
