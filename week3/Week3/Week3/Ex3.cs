﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week3
{
    public static class Extensions
    {
       public static T Sum<T>(this IEnumerable<T> enum1,T value1,T value2)
        {
            dynamic returnValue = (dynamic)value1 +(dynamic)value2;
            
            return returnValue;
        }

        public static T Product<T>(this IEnumerable<T> enum1, T value1, T value2)
        {
            dynamic returnValue = (dynamic)value1 + (dynamic)value2;

            return returnValue;
        }

        public static T Min<T>(this IEnumerable<T> enumerable)
        {
            return enumerable.Min<T>();
        }

        public static T Max<T>(this IEnumerable<T> enumerable)
        {
            return enumerable.Max<T>();
        }

        public static T Average<T>(this IEnumerable<T> enumerable)
        {
            return enumerable.Average<T>();
        }

    }
}
