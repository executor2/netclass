﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week3
{
    class InvalidRangeException<T> : System.Exception
    {
        private static readonly string DefaultMessage = "Invalid range";
        private string ErrorMessage { get; set; }
        private T Min { get; set; }
        private T Max { get; set; }


        public InvalidRangeException() : base(DefaultMessage) { }

        public InvalidRangeException(string? message) : base(message) { }

        public InvalidRangeException(string? message, Exception? innerException) : base(message, innerException) { }

        public InvalidRangeException(string errorMessage, T min, T max)
        {
            ErrorMessage = errorMessage;
            Min = min;
            Max = Max;
        }
    }
}
