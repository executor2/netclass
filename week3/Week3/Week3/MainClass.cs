﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Week3
{
    class MainClass
    {
        static void Main(string[] args)
        {
            /* Ex1
             * 
            <string> tString = new Ex1<string>();
            tString.Add("a");
            tString.Add("b");
            tString.RemoveAt(1);
            tString.ToString();
            */

            /*
             * Ex2
            Timer t = new Timer();
            */

            /* Ex3
           List<int> enumerable = new List<int>();
            int IntValue = enumerable.Sum(1,2);
            Console.WriteLine(IntValue);

            enumerable.Add(1);
            enumerable.Add(2);

            Console.WriteLine(enumerable.Min<int>());
            Console.WriteLine(enumerable.Max<int>());

            List<double> enumFloat = new List<double>();
            double floatValue = enumFloat.Sum(2.0, 4.0);
            
            
            Console.WriteLine(floatValue);
            */

            /*
             * Ex4
            int[] numbers = new int[100];

            try
            {
                numbers[101] = 5;
            }catch(Exception e)
            {
                throw new InvalidRangeException<int>("Number out of range", 1, 100);
            }
            */

            /*
            Ex5 ex5 = new Ex5();
            ex5.ReadNumber(1, 100);
            */

            BitArray64 c1 = new BitArray64(1);
            BitArray64 c2 = new BitArray64(2);

            bool isTrue = c1.Equals(c2);

            int index = c2[1];

            Console.WriteLine( isTrue);
            Console.WriteLine(c1==c2);
            Console.WriteLine(c1 != c2);
            Console.WriteLine($"index {index}");
            Console.WriteLine(c1.GetHashCode());
            Console.WriteLine(c2.GetHashCode());
        }

        public static void doPrint(string text)
        {
            Console.WriteLine("Print something special: " + text);
        }
    }


}
