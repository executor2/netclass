﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week3
{
    class Ex5
    {
        int index;
        int[] numbers = new int[10];

        public void ReadNumber(int start, int end)
        {
            Console.WriteLine("Enter 10 numbers:");

            for (int i = 0; i < 10; i++)
            {
                var inputValue = Console.ReadLine();
                int number;

                if (!int.TryParse(inputValue, out number))
                {
                    throw new Exception("Not a number");
                }
                if(i == 0)
                {
                    if(number > end || number < start)
                    {
                        throw new Exception("inequality is not true");
                    }
                }else if (number <= numbers[index-1]
                    || number > end
                    || number < start)
                {
                    throw new Exception("inequality is not true");
                }

                numbers[index] = number;
                index++;                
            }

            Console.WriteLine(1);
            for( int i = 0; i < 10; i++)
            {
                
                Console.Write($"{numbers[i]} < ");
            }
            Console.WriteLine(100);
        }
    }
}
