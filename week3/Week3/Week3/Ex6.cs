﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Week3
{
    class BitArray64 : IEnumerable<int>
    {
        private ulong Value { get; set; }

        public BitArray64(ulong number)
        {
            Value = number;
        }
        public int this[int index]
        {
            get
            {
                return (Value & (1ul << index)) == 0 ? 0 : 1;
            }

            set
            {
                Value &= ~((uint)(1 << index));

                Value |= (uint)(value << index);
            }
        }

        public IEnumerator<int> GetEnumerator()
        {
            int index = 0;

            while (index < 64)
            {
                yield return (Value & (1ul << index)) == 0 ? 0 : 1;
                index++;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public override bool Equals(Object objArray)
        {
            int index = 0;

            BitArray64 bArray = (BitArray64)objArray;

            while (index < 64)
            {
                if (this[index] != bArray[index])
                {
                    return false;
                }
                index++;
            }

            return true;
        }

        public static bool operator ==(BitArray64 m1, BitArray64 m2)
        {
            return m1.Equals(m2);
        }

        public static bool operator !=(BitArray64 m1, BitArray64 m2)
        {
            return !m1.Equals(m2);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

    }
}
