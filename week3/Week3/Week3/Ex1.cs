﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week3
{
    class Ex1<T>
    {
        /*
            list.Add("a");
            list.Remove("a");            
            list.Clear();
            list.ToArray();          
         */

        private T[] container;
        private int capacity;
        private int currentCapacity;
        private int currentIndex;


        public Ex1()
        {            
            capacity = currentCapacity= 8;
            currentIndex = 0;
            container = new T[capacity];
        }

        public void Add(T item)
        {
            
            if(currentCapacity >= capacity)
            {
                T[] newContainer = new T[capacity * 2];
                
                for(int i = 0; i < capacity; i++)
                {                    
                    newContainer[i] = container[i];
                }

                container = newContainer;
            }

            container[currentIndex] = item;
            currentIndex++;
            capacity++;  
           
        }

        public void RemoveAt(int index)
        {
            if (index < capacity)
            {
                Array.Copy(container, index + 1, container, index, capacity - index);
            }
            container[capacity] = default(T);
        }

        public void Clear()
        {
            container = new T[capacity];
        }

        public void ToString()
        {
            foreach (T item in container)
            {
                Console.WriteLine(item);
            }
        }

    }
}
