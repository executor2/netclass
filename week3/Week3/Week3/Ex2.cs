﻿using System;
using System.Threading;

namespace Week3
{
    public delegate void DoPrint();

    class Timer
    {
        private int seconds;
        public Timer()
        {
            DoPrint printSomething = new DoPrint(ShowMessage);

            while (seconds <= 10)
            {
                printSomething();
                seconds++;
                Thread.Sleep(1000);
            }
        }

        private void ShowMessage()
        {
            Console.WriteLine("One message " + seconds);
        }
    }
}
